import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { App } from '@capacitor/app';
import Overview from 'src/plugins/overview/overview-plugin';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {
  constructor(private platform: Platform) {
    // this.initializeApp();
  }

  // initializeApp() {
  //   this.platform.ready().then(() => {
  //     App.addListener('appStateChange', (state) => {
  //       if (state.isActive) {
  //         // La aplicación está en primer plano
  //         console.log('La aplicación está en primer plano', state.isActive);
  //         Overview.simulateWindowFocusChanged({ hasFocus: true });
  //       } else {
  //         // La aplicación está en segundo plano
  //         console.log('La aplicación está en primer plano', state.isActive);
  //         Overview.simulateWindowFocusChanged({ hasFocus: false });
  //       }
  //     });
  //   });
  // }
}

import { registerPlugin } from '@capacitor/core';

import type { OverviewPlugin } from './definitions';

const Overview = registerPlugin<OverviewPlugin>('OverviewPlugin', {
  web: () => import('./web').then(m => new m.OverviewWeb()),
});

export * from './definitions';
export default Overview ;

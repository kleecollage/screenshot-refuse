export interface OverviewPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  simulateWindowFocusChanged(callback: WindowFocusChangeEvent): Promise<void>;
}

export interface WindowFocusChangeEvent {
  hasFocus: boolean;
}

export type CallbackID = string;

export interface MyData {
  data: string;
}

export type MyPluginCallback = (message: MyData | null, err?: any) => void;

export interface MyPlugin {
  method1(): Promise<void>;
  method2(): Promise<MyData>;
  cmethod3(callback: MyPluginCallback): Promise<CallbackID>;
}

